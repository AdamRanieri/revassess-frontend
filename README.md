# RevAssess

## Project Description

RevAssess is a software challenge platform. Whereas a coding challenge requires one to implement a method RevAssess software challenges are more holistic. Challenges can include creating a RESTful web service or debugging a large application. Trainers are able to create code skeletons and put them on GitHub or GitLab. Associates can clone these repos and attempt the software challenge. Each time tests on run on an Associate's local machine results are sent to RevAssess. Trainers can review these results to provided targeted feedback.

Below is a link to the overview of what the RevAssess software does.

!(https://gitlab.com/AdamRanieri/node-gcp-batch/-/blob/main/RevAssess.md)

## Technologies Used

* GCP Kubernetes Engine
* Docker
* Terraform
* NodeJS
* Cloud SQL
* Cloud Function
* Cloud Pub/Sub
* Cloud Run
* Cloud Datastore
* Check each services setup.md for more specific technologies used.

## Features

List of features ready and TODOs for future development
* An assessment ingestion service that allows associates to submit their assessments through pub/sub
* A Cloud Function that uploads zipped source code to cloud storage and the assessments & their tests to Cloud SQL
* A REST API where only trainers can submit exercises to Cloud SQL and obtain information on associates assessments for each exercise
* An authorization service that returns a JWT that will be passed to the REST API in order to access trainer specific services
* A frontend built in React and Hosted on Firebase using material ui


## Getting Started

* Check the setup.md in each for the specifics of deployment
   
### Frontend
```bash
git clone https://gitlab.com/AdamRanieri/revassess-frontend.git
```
### REST API
```bash
git clone https://gitlab.com/AdamRanieri/revassess-excercise-rest-api.git
```
### Assessment Ingetion
```bash
git clone https://gitlab.com/AdamRanieri/revassess-ingestion-service.git
```
### Uploader Function
```bash
git clone https://gitlab.com/AdamRanieri/revassess-uploader-function.git
```
### Authorization
```bash
git clone https://gitlab.com/AdamRanieri/revassess-authorization-service.git
```
## Contributer
> Aishwarya Naik
> Anthony Renwick
> Camden Snyder
> Donavan Merrin
> Michelle Pierce
> Rafael Mora
> Alejandra Cajiao
> Jesse Esquivel
> Aldrin Caalim
> Michael Eyo
> James Mitchell Kirk
> Sandra Muszynski
> Willis Larson
> Jarrick Hillery
> Phong Vo
> Josh Forcier
> Thomas Smithey
> Brystan Baeder
> Alan Tillmann
> Charles Jester


